/**
 * Top level user bank account info interface for an API result.
 * This is populated by querying Plaid using existing BoomUserPlaidInfo data retrieved from a Firebase profile
 */
export interface UserBankAccountInfo {
  relatedRecordId?: string;
  accountName: string;
  accountBalance: AccountBalanceInfo;
  accountNumber: PlaidAuthAccountNumbers;
  memberId?: string;
  memberFullName: string;
  memberAddress: string;
  memberPhoneNumber: string;
}

/**
 * The top level interface for a Boom User's plaid info, as saved in their Firebase profile. This info is partial and doesn't
 * contain full bank account info for security reasons. This data is used to query Plaid to retreive full account info.
 */
export interface BoomUserPlaidInfo {
  /**
   * Likely not being used
   */
  account: { id: any; mask: any; name: any; subtype: any; type: any };
  /**
   * Likely not being used
   */
  account_id: any | null;
  accounts: BoomUserPlaidAccount[];
  institution: { institution_id: string; name: string };
  item: { accessToken: string; itemId: string };
  link_session_id: string;
  public_token: string;
}

/**
 * Information about a single bank account.
 * Example:
 * {
 *  id: 'yBBlWogGGwuZRMB3ZB5MUQGWl5r1N7Iy1lXRL',
 *  mask: '0000'
 *  name: 'Checking'
 *  subtype: 'checking'
 *  type: 'depository'
 * }
 *
 * Note: We don't store bank account numbers, only object references to Plaid. This is to follow best security practices.
 */
export interface BoomUserPlaidAccount {
  id: string;
  mask: string;
  name: string;
  subtype: string;
  type: string;
}

/**
 * Item holding actual bank account number and routing information in clear text
 */
export interface PlaidAuthAccountNumberInfo {
  account: string;
  account_id: string;
  routing: string;
  wire_routing: string;
}

/**
 * Collection of the different types of account number info items that can be retrieved
 */
export interface PlaidAuthAccountNumbers {
  achNumbers: PlaidAuthAccountNumberInfo[];
  eftNumbers: PlaidAuthAccountNumberInfo[];
  internationalNumbers: PlaidAuthAccountNumberInfo[];
  bacsNumbers: PlaidAuthAccountNumberInfo[];
}

export interface AccountBalanceInfo {
  available: number;
  current: number;
  iso_currency_code: string;
  limit: number | null;
  unofficial_currency_code: string | null;
}

/**
 * The result of a Plaid service auth API request
 */
export interface PlaidAuthResult {
  numbers: PlaidAuthAccountNumbers;
  accounts: any;
}
