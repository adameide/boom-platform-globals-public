export interface ContactInfo {
  emails?: string[] | null | undefined;
  phoneNumber: string;
  address?: string | null | undefined;
  city?: string | null | undefined;
  state?: string | null | undefined;
  zipcode?: string | null | undefined;
  country?: string | null | undefined;
}
