export enum TransactionType {
  FUNDING = 'funding',
  PURCHASE = 'purchase',
  TRANSFER = 'transfer',
  RETURN = 'return',
  MERCHANT_WITHDRAWAL = 'merchant-withdrawal',
}
export enum TransactionStatus {
  PENDING = 'pending',
  CANCELLED = 'cancelled',
  COMPLETED = 'completed',
  FAILED = 'failed',
  UNPROCESSED = 'unprocessed',
}
