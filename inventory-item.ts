import { BoomUser } from './user';
import { Store } from './store';
import { InventoryItemStatus, InventoryItemInactiveReason } from './enums/inventory';
import { Money } from './money';

export interface InventoryItem {
  friendlyID?: string;
  itemID?: string;
  itemType?: string;
  itemName?: string;
  nickname?: string;
  merchant?: Partial<BoomUser>;
  store?: Partial<Store>;
  status?: InventoryItemStatus;
  purchasePrice?: Money;
  inactiveReason?: InventoryItemInactiveReason;
}
