import { ModelBase } from './base-record';
import { InventoryItem } from './inventory-item';
import { Money } from './money';
import { BoomUser } from './user';
import { FulfillmentStatus } from './enums/inventory';

export interface InventoryLease extends ModelBase {
  inventoryItem: Partial<InventoryItem>;
  leaseAmount: Money;
  leaseExpiration: number;
  fulfillmentAmount: Money;
  amountPaid: Money;
  merchant: Partial<BoomUser>;
  fulfillmentStatus: FulfillmentStatus;
}
