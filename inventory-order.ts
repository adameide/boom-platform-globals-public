import { Money } from './money';
import { BoomUser } from './user';
import { Store } from './store';
import { InventoryItem } from './inventory-item';
import {
  InventoryOrderStatus,
  InventoryOrderBillingType,
  InventoryOrderType,
} from './enums/inventory';

export interface InventoryOrder {
  _id?: string;
  createdAt?: number;
  updatedAt?: number;
  item?: InventoryItem;
  status?: InventoryOrderStatus;
  billingType?: InventoryOrderBillingType;
  orderType?: InventoryOrderType;
  amount?: Money;
  merchant?: Partial<BoomUser>;
  store?: Partial<Store>;
  notes?: string;
}
