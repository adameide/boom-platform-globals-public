import { ModelBase } from './base-record';
import { Money } from './money';
import { BoomUserBasic } from './user';
import { Store } from './store';
import { InventoryItem } from './inventory-item';
import { InventoryLease } from './inventory-lease';
import { MerchantTransactionStatus, MerchantTransactionType } from './enums/merchant-transaction';

export interface MerchantTransaction extends ModelBase {
  title: string;
  status: MerchantTransactionStatus;
  type: MerchantTransactionType;
  salestax?: Money;
  salestaxState?: 'AL|AK|AS|AZ|AR|CA|CO|CT|DE|DC|FM|FL|GA|GU|HI|ID|IL|IN|IA|KS|KY|LA|ME|MH|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|MP|OH|OK|OR|PW|PA|PR|RI|SC|SD|TN|TX|UT|VT|VI|VA|WA|WV|WI|WY';
  amount?: Money;
  merchant: BoomUserBasic;
  store: Store;
  purchaseItem: Partial<InventoryItem>;
  inventoryLease?: Partial<InventoryLease>;
}
