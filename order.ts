import { InventoryOrder } from './inventory-order';
import { MerchantTransaction } from './merchant-transaction';

export interface InventoryOrderResult {
  success: boolean;
  message: string;
  inventoryOrders?: InventoryOrder[];
  merchantTransactions?: MerchantTransaction[];
}
