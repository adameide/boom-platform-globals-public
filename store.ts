import { ModelBase } from './base-record';
import { BoomUser } from './user';
import { ContactInfo } from './contact-info';
import { Geolocation } from './geolocation';

export interface Store extends ModelBase, ContactInfo {
  pin?: number | string;
  objectID?: string;
  companyLogoUrl?: string;
  coverImageUrl?: string;
  companyType?: string;
  companyName?: string;
  companyDescription?: string;
  links?: string[] | null | undefined;
  _tags?: string[];
  _geoloc: Geolocation;
  openingTime?: number;
  closingTime?: number;
  days?: string[];
  merchant?: Partial<BoomUser>;
}
