import { ContactInfo } from './contact-info';
import { Money } from './money';
import { RoleKey } from './enums/role-key';
import { Store } from './store';
import { BoomUserPlaidInfo } from './bank-info';

export interface BoomUserBasic {
  uid: string;
  firstName?: string;
  lastName?: string;
  contact: Partial<ContactInfo>;
}
export interface BoomUser extends BoomUserBasic {
  createdAt: number;
  updatedAt: number;
  lastName?: string;
  gender?: string;
  registrationStep?: number;
  finishedRegistration?: boolean;
  roles: RoleKey[];
  cards?: string[] | undefined | null;
  store?: Partial<Store>;
  profileImgUrl?: string;
  enableNotification?: boolean;
  notificationSound?: boolean;
  notificationVibration?: boolean;
  fcmToken?: string;
  range?: number;
  grossEarningsPendingWithdrawal?: Money;
  netEarningsPendingWithdrawal?: Money;
  tempPassword?: string;
  password: string;
  plaidInfo: BoomUserPlaidInfo[];
}

export enum AccountInfoQueryTypes {
  BoomcardPin = 'boom-card-pin',
  UsernamePassword = 'username-password',
}
